from matplotlib import pyplot as plt
import numpy as np
from matplotlib.animation import PillowWriter
from utils import set_default

MAKE_GIF = True

def get_rotation_matrix(theta):
    return np.array([
        [np.cos(theta), -np.sin(theta)], 
        [np.sin(theta), np.cos(theta)]
    ]).T

def plot_base(v, c):
    a = ax.arrow(o[0], o[1], v[0], v[1], width=0.04, color=c, length_includes_head=True, head_width=0.1, head_length=0.1)
    return a

set_default(figsize=(14, 14))
metadata = dict(title='Movie', artist='tz')
writer = PillowWriter(fps=30, metadata=metadata)

fig, ax = plt.subplots()
ax.set_xlim(-2,2)
ax.set_ylim(-2,2)

o = np.array([0, 0])
x = np.array([1, 0])
y = np.array([0, 1])

c = ['red', 'green']

a = plot_base(x, 'red')
b = plot_base(y, 'green')

plt.draw()

if MAKE_GIF:
    angles = np.linspace(0, 2*np.pi, 200)
    with writer.saving(fig, "outputs/2d.gif", 100):
        for i in angles:
            a.remove()
            b.remove()
            R = get_rotation_matrix(i)
            x_ = np.matmul(R, x)
            y_ = np.matmul(R, y)
            a = plot_base(x_, 'red')
            b = plot_base(y_, 'green')
            writer.grab_frame()
else:
    plt.show()
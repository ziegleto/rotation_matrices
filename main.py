from matplotlib import pyplot as plt
import numpy as np
from matplotlib.animation import PillowWriter
from utils import set_default, get_pitch_matrix, get_roll_matrix, get_yaw_matrix
from matplotlib.colors import Normalize
import matplotlib.cm as cm

#MAKE_GIF = True
set_default()

def plot_bases(ax, V, C):
    a = ax.quiver(0, 0, 0, V[:, 0], V[:, 1], V[:, 2], color = C)
    return a

def draw_rotation(r_matrix_fn, fname, make_gif=True):
    metadata = dict(title='Movie', artist='tz')
    writer = PillowWriter(fps=15, metadata=metadata)

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)


    V = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ])
    c = 'salmon'
    a = plot_bases(ax, V, c)

    plt.draw()

    if make_gif:
        angles = np.linspace(0, 2*np.pi, 30)
        with writer.saving(fig, f"outputs/{fname}", 100): 
            for i in angles:
                a.remove()
                R = r_matrix_fn(i)
                v_ = np.matmul(R, V)
                a = plot_bases(ax, v_, c)
                writer.grab_frame()
            for j in range(10):
                writer.grab_frame()
    else:
        plt.show()

def main():
    draw_rotation(get_pitch_matrix, 'pitch.gif')
    draw_rotation(get_yaw_matrix, 'yaw.gif')
    draw_rotation(get_roll_matrix, 'roll.gif')

if __name__=='__main__':
    main()
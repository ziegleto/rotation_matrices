# Readme

The code in this repository is straightforward. It visualizes rotation matrices, e.g., 
pitch, raw, roll in 3d, by using `PillowWriter`.  The matrices are defined in the `utils.py` file. `main.py` visualizes pitch, yaw, and roll, respectively as gifs in the `outputs` directory. The `2d` file visualizes an one-dimensional rotation.

## Examples

### Roll
![roll](outputs/roll.gif)

### 2D
![2d](outputs/2d.gif)